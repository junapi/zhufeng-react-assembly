今天我们写upload组件

这个组件有点繁琐，先梳理下逻辑。

## 逻辑梳理

这个上传可以定制的条件太多了，主要先做成3种，一种按钮上传，一种列表上传，一种图片上传。

按钮上传需要在按钮上显示上传结果。

列表上传可以显示上传列表，列表显示上传结果。

图片上传可以进行图像裁剪，然后上传裁剪后的图像。

## 搭建后台

为了方便测试，简单用express做个后台：

项目外找个文件夹init -y

```
cnpm i typescript express  @types/express multer @types/multer ts-node-dev  cors @types/cors  -S
```

```
npx tsconfig.json
```

然后选择node。

修改package.json

```
  "scripts": {
    "build": "tsc",
    "start": "ts-node-dev --respawn src/index.ts"
  },
```

然后src下建index.ts

```typescript
import express, { Request, Response, NextFunction } from "express";
import cors from "cors";
import path from "path";
import multer from "multer";

const PORT = 51111;

const storage = multer.diskStorage({
	destination: path.join(__dirname, "public", "uploads"),
	filename(_req: Request, file: Express.Multer.File, cb) {
		cb(null, Date.now() + path.extname(file.originalname));
	},
});

const upload = multer({ storage });
const app = express();
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get("/", (_req, res, _next) => {
	res.json({ success: true, data: "xx" });
});

app.post(
	"/user/uploadAvatar",
	upload.single("avatar"),
	(req: Request, res: Response, _next: NextFunction) => {
		let domain = "localhost";
		let avatar = `http://${domain}:${PORT}/uploads/${req.file.filename}`;
		res.send({ success: true, data: avatar });
	}
);

(async function () {
	app.listen(PORT, () => {
		console.log(`running on http://localhost:${PORT}`);
	});
})();
```

然后启动它。

使用工具比如insominia上传个文件试试。

post 地址：http://localhost:51111/user/uploadAvatar/

格式选择multipart form 

字段传avatar 然后value就是选个file。

发送后会返回：

```
{
  "success": true,
  "data": "http://localhost:51111/uploads/1596019074057.dll"
}
```

然后看看src文件夹里是不是多了刚刚上传的文件。都ok说明完成了。

## 基本框架

我们有3个需要制作的逻辑，3个逻辑都是复用的一个input框。

```typescript
export function Upload(props: PropsWithChildren<UploadProps>) {
	const inputRef = useRef<HTMLInputElement>(null);
	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (e.target.files && e.target.files.length <= 0) return;
		console.log(e.target.files);
	};
	const handleClick = () => {
		inputRef.current?.click();
	};

	return (
		<div>
			<input
				ref={inputRef}
				type="file"
				onChange={handleChange}
				style={{ display: "none" }}
				value=""
			></input>
			<Button onClick={handleClick}>upload</Button>
		</div>
	);
}
```

这里有个bug，就是value没有值得话，选同一张照片不会触发onchange事件。

这里第一步就完成了 ，下面需要将文件上传至服务器，需要安装axios。

 首先安装axios ，然后试着用axios上传，看能不能传成功：

```typescript
export function Upload(props: PropsWithChildren<UploadProps>) {
	const inputRef = useRef<HTMLInputElement>(null);
	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (e.target.files && e.target.files.length <= 0) return;
		console.log(e.target.files);
		const formData = new FormData();
		formData.append("avatar", e.target.files![0]);
		axios.post("http://localhost:51111/user/uploadAvatar/", formData, {
			headers: {
				"Content-Type": "multipart/form-data",
			},
		});
	};
	const handleClick = () => {
		inputRef.current?.click();
	};

	return (
		<div>
			<input
				ref={inputRef}
				type="file"
				onChange={handleChange}
				style={{ display: "none" }}
				value=""
			></input>
			<Button onClick={handleClick}>upload</Button>
		</div>
	);
}
```

如果能成功说明ok，开始拆分函数。

首先，先把请求给拆出来，由于配置项太多，我们可以直接设定是axios配置项，然后使用axios配置进行调用。

另外，上传进度也在axios的配置项里，利用回调，可以得到上传进度，同时做出进度条所要的数据：

```typescript
export const updateFilist = (
	setFlist: React.Dispatch<React.SetStateAction<ProgressBar[]>>,
	_file: ProgressBar,
	uploadPartial: Partial<ProgressBar>
) => {
	setFlist((prevList) => {
		if (prevList) {
			return prevList.map((v) => {
				if (v.uid === _file.uid) {
					return { ...v, ...uploadPartial };
				} else {
					return v;
				}
			});
		} else {
			return prevList;
		}
	});
};

interface ProgressBar {
	filename: string;
	percent: number;
	status: "ready" | "success" | "failed" | "upload";
	uid: string;
	size: number;
	raw: File | null;
	cancel?: CancelTokenSource;
	img?: string | ArrayBuffer | null;
}
type onProgressType = ((p: number, f: File, i: number) => void) | undefined;

const postData = function(
	file: File,
	filename: string,
	config: Partial<AxiosRequestConfig>,
	i: number, //多重上传时i用来标识第几个
	onProgress: onProgressType,
	setFlist: React.Dispatch<React.SetStateAction<ProgressBar[]>>,
	successCallback: ((res: any, i: number) => void) | undefined,
	failCallback: ((res: any, i: number) => void) | undefined
) {
	const formData = new FormData();
	formData.append(filename, file);
	const source = axios.CancelToken.source();
	const _file: ProgressBar = {
		filename: file.name,
		percent: 0,
		status: "ready",
		uid: Date.now() + "upload",
		size: file.size,
		raw: file,
		cancel: source,
	};
	setFlist((prev) => {
		//添加进队列
		return [_file, ...prev];
	});
	const defaultAxiosConfig: Partial<AxiosRequestConfig> = {
		method: "post",
		url: "http://localhost:51111/user/uploadAvatar/",
		data: formData,
		headers: {
			"Content-Type": "multipart/form-data",
		},
		cancelToken: source.token,
		onUploadProgress: (e) => {
			let percentage = Math.round((e.loaded * 100) / e.total) || 0;
			updateFilist(setFlist, _file, {
				status: "upload",
				percent: percentage,
			}); //更新上传队列
			if (onProgress) {
				onProgress(percentage, file, i);
			}
		},
	};
	const mergeConfig = { ...defaultAxiosConfig, ...config };

	return axios(mergeConfig)
		.then((res) => {
			updateFilist(setFlist, _file, { status: "success", percent: 100 });
			if (successCallback) {
				successCallback(res, i);
			}
		})
		.catch((r) => {
			updateFilist(setFlist, _file, { status: "failed", percent: 0 });
			if (failCallback) {
				failCallback(r, i);
			}
		});
};

const resolveFilename = function(
	uploadFilename: string[] | string,
	index: number
) {
	if (Array.isArray(uploadFilename)) {
		return uploadFilename[index];
	} else {
		return uploadFilename;
	}
};

type UploadProps = {
	/** 上传字段名*/
	uploadFilename: string[] | string;
	/** 发送设置，参考axios */
	axiosConfig?: Partial<AxiosRequestConfig>;
	/** 获取进度 */
	onProgress?: onProgressType;
	/** 成功回调 */
	successCallback?: ((res: any, i: number) => void) | undefined;
	/** 失败回调 */
	failCallback?: ((res: any, i: number) => void) | undefined;
	/** 上传列表初始值 */
	defaultProgressBar?: ProgressBar[];
};

export function Upload(props: UploadProps) {
	const {
		axiosConfig,
		onProgress,
		defaultProgressBar,
		uploadFilename,
		successCallback,
		failCallback,
	} = props;
	const [flist, setFlist] = useState<ProgressBar[]>(defaultProgressBar || []);
	const inputRef = useRef<HTMLInputElement>(null);
	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (!e.target.files) return;
		if (e.target.files && e.target.files.length <= 0) return;
		let filelist = Array.from(e.target.files);
		filelist.forEach((f, i) => {
			postData(
				f,
				resolveFilename(uploadFilename, i),
				axiosConfig!,
				i,
				onProgress,
				setFlist,
				successCallback,
				failCallback
			);
		});
	};
	const handleClick = () => {
		inputRef.current?.click();
	};

	return (
		<div>
			<input
				ref={inputRef}
				type="file"
				onChange={handleChange}
				style={{ display: "none" }}
				value=""
			></input>
			<Button onClick={handleClick}>upload</Button>
			{flist.map((v) => {
				return (
					<div key={v.uid}>
						{v.filename}
						{v.percent}
						{v.status}
					</div>
				);
			})}
		</div>
	);
}
Upload.defaultProps = {
	axiosConfig: {},
	uploadFilename: "avatar",
};
```

可以上传几个文件看看，如果正常就ok。

关于修改文件名的问题，这个靠后端改，因为file里的name字段是只读的，如果要修改需要重新构造个file，所以传过去后端改比较好点。

下面增加beforeUpload逻辑。一般上传会有个beforeUpload，来做验证或者搞点别的，这个有可能是异步的，所以需要都判断好。

```
	/** 如果返回promise，需要提供file，否则同步需要返回boolean，如果为false，则不发送*/
	beforeUpload?: (f: File, i: number) => boolean | Promise<File>;
```

在循环里加入下面逻辑：

```typescript
	filelist.forEach((f, i) => {
			if(beforeUpload){
				const p=beforeUpload(f,i)
				if(p instanceof Promise){
					p.then((res:File)=>{
						postData(
							res,
							resolveFilename(uploadFilename, i),
							axiosConfig!,
							i,
							onProgress,
							setFlist,
							successCallback,
							failCallback
						);
					})
				}else{
					if(p){//false不执行
						postData(
							f,
							resolveFilename(uploadFilename, i),
							axiosConfig!,
							i,
							onProgress,
							setFlist,
							successCallback,
							failCallback
						);
					}
				}
			}else{
				postData(
					f,
					resolveFilename(uploadFilename, i),
					axiosConfig!,
					i,
					onProgress,
					setFlist,
					successCallback,
					failCallback
				);
			}
		
		});
```

为了使得上传成功与否有提示，我们可以借助先前配置的suceessCallback与failCallback配置默认值，就是利用前面做的message组件：

```
Upload.defaultProps = {
	axiosConfig: {},
	uploadFilename: "avatar",
	successCallback: () => message.success("上传成功"),
	failCallbakc: () => message.error("上传失败"),
};
```

由于我们要写3种模式上传，第一种模式只有个button，文件在上传中体验不好，所以我们需要做个判断，利用上传时给flist赋了个状态改一下：

```
const resolveBtnLoading = function(flist: ProgressBar[]) {
	return flist.some((v) => v.status === "upload");
};
```

btn 改一下：

```
	<Button
				onClick={handleClick}
				isLoading={resolveBtnLoading(flist)}
				loadingText="上传中..."
			>
				upload
			</Button>
```

这样只有按钮时就舒服多了。文件在上传时会展示上传的样子，且上传失败或者成功都有对应显示。

下面将3种模式分开来。由于图片也可以显示上传列表，所以我们需要把列表单独做配置，只设置2种模式。

```
type UploadMode = 'default'|'img'
```

配置默认选default，

暴露出progress用来开启进度列表：

```
/** 上传模式 2种 */
	uploadMode?:UploadMode
	/** 是否开启进度列表 */
	progress?:boolean
```

然后就可以进行区分。

进度列表方面，只有progress为true显示,img模式必显示。

img模式下btn会换成特制的上传按钮，这里就有个上传上限问题，这个比较好解决，到上限不出现上传按钮即可。剩下的就是列表问题，列表要回显图片，做成和上传一样的形状。

我们把列表单独抽成一个组件进行管理，为了方便，default列表和img列表先写成不同组件。

```typescript
interface UploadListProps {
	flist: ProgressBar[];
	onRemove: (item: ProgressBar) => void;
}

function UploadList(props: UploadListProps) {
	const { flist, onRemove } = props;
	return (
		<ul>
			{flist.map((item) => {
				return (
					<li key={item.uid}>
						{item.filename}
						{(item.status === "upload" ||
							item.status === "ready") && (
							<Progress count={item.percent}></Progress>
						)}
					</li>
				);
			})}
		</ul>
	);
}

interface imageListProps extends UploadListProps {
	setFlist: React.Dispatch<React.SetStateAction<ProgressBar[]>>;
}

export function ImageList(props: imageListProps) {
	const { flist, onRemove, setFlist } = props;
	useMemo(() => {
		if (flist) {
			flist.forEach((item) => {
				if (item.raw && !item.img) {//如果有文件并且没有img地址，生成blob地址
					const reader = new FileReader();
					reader.addEventListener("load", () => {
						updateFilist(setFlist, item, {
							img: reader.result || "error",
						});
					});
					reader.readAsDataURL(item.raw);
				}
			});
		}
	}, [flist, setFlist]);
	return (
		<div>
			{flist.map((item) => {
				return (
					<span key={item.uid}>
						<img src={item.img as string} alt="3"></img>
					</span>
				);
			})}
		</div>
	);
}

```

其中，onRemove是删除列表的操作，目前列表元素内删除还没制作，所以先空着。

渲染条件就可以这么判断：

```typescript
		{uploadMode === "default" && progress && (
				<UploadList flist={flist} onRemove={onRemove}></UploadList>
			)}
			{uploadMode === "img" && (
				<ImageList
					flist={flist}
					setFlist={setFlist}
					onRemove={onRemove}
				></ImageList>
			)}
```

下面需要做个图片上传的按钮，并且图片也要按照类似上传按钮大小显示，同时，完成hover时，会显示个垃圾桶图标，然后绑定onRemove方法：

```typescript
const ImgWrapper = styled.div`
	display: inline-block;
	position: relative;
	width: 104px;
	height: 104px;
	margin-right: 8px;
	margin-bottom: 8px;
	text-align: center;
	vertical-align: top;
	background-color: #fafafa;
	border: 1px dashed #d9d9d9;
	border-radius: 2px;
	cursor: pointer;
	transition: border-color 0.3s ease;
	> img {
		width: 100%;
		height: 100%;
	}

	&::before {
		position: absolute;
		z-index: 1;
		width: 100%;
		height: 100%;
		background-color: rgba(0, 0, 0, 0.5);
		opacity: 0;
		-webkit-transition: all 0.3s;
		transition: all 0.3s;
		content: " ";
	}
	&:hover::before {
		position: absolute;
		z-index: 1;
		width: 100%;
		height: 100%;
		background-color: rgba(0, 0, 0, 0.5);
		opacity: 1;
		-webkit-transition: all 0.3s;
		transition: all 0.3s;
		content: " ";
	}
	&:hover > .closebtn {
		display: block;
	}
`;

const ImgCloseBtn = styled.div`
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	z-index: 2;
	display: none;
`;
```

图片list的渲染返回这么改：

```typescript
return (
		<React.Fragment>
			{flist.map((item) => {
				return (
					<span key={item.uid}>
						<ImgWrapper>
							<img src={item.img as string} alt="3"></img>
							<ImgCloseBtn
								className="closebtn"
								onClick={() => onRemove(item)}
							>
								<Icon icon="trash" color={color.light}></Icon>
							</ImgCloseBtn>
						</ImgWrapper>
					</span>
				);
			})}
		</React.Fragment>
	);
```

onRemove方法，其中customRemove和onRemoveCallback暴露出接口：

```typescript
onRemoveCallback?: (f: ProgressBar) => void;
	/** 自定义删除行为，只有img与progress为true有效*/
	customRemove?: (
		file: ProgressBar,
		setFlist: React.Dispatch<React.SetStateAction<ProgressBar[]>>
	) => void;
```

```typescript
	const onRemove = useCallback(
		(file: ProgressBar) => {
			if (customRemove) {
				customRemove(file, setFlist);
			} else {
				setFlist((prev) => {
					return prev.filter((item) => {
						if (
							item.uid === file.uid &&
							item.status === "upload" &&
							item.cancel
						) {
							item.cancel.cancel();
						}
						return item.uid !== file.uid;
					});
				});
			}

			if (onRemoveCallback) {
				onRemoveCallback(file);
			}
		},
		[customRemove, onRemoveCallback]
	);
```

这样可以试试，如果上传图片后，鼠标hover过去显示个垃圾桶，并且点击垃圾桶可以删除就ok。

做完了图片上传的关闭按钮，下面制作progress的关闭按钮。

首先，制作把progress的li变成左右布局，然后内部做个icon可以close掉:

```
const ProgressListItem = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
`;
const ProgressLi = styled.li`
	list-style: none;
	padding: 10px;
	box-shadow: 2px 2px 4px #d9d9d9;
`;
```

```typescript
function UploadList(props: UploadListProps) {
	const { flist, onRemove } = props;
	return (
		<ul style={{ padding: "10px" }}>
			{flist.map((item) => {
				return (
					<ProgressLi key={item.uid}>
						<ProgressListItem>
							<div>{item.filename}</div>
							<div>
								<Button
									style={{
										padding: "0",
										background: "transparent",
									}}
									onClick={() => onRemove(item)}
								>
									<Icon icon="close"></Icon>
								</Button>
							</div>
						</ProgressListItem>

						{(item.status === "upload" ||
							item.status === "ready") && (
							<Progress count={item.percent}></Progress>
						)}
					</ProgressLi>
				);
			})}
		</ul>
	);
}
```

这样就完成了。

下面我们需要制作图片上传的按钮。

图片上传按钮应该跟显示图片一个大小。

```typescript
const ImgUpload = styled.div`
	display: inline-block;
	position: relative;
	width: 104px;
	height: 104px;
	margin-right: 8px;
	margin-bottom: 8px;
	text-align: center;
	vertical-align: top;
	background-color: #fafafa;
	border: 1px dashed #d9d9d9;
	border-radius: 2px;
	cursor: pointer;
	transition: border-color 0.3s ease;
	> svg {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
`;
```

渲染部分修改下：

```tsx
			{uploadMode === "default" && (
				<Button
					onClick={handleClick}
					isLoading={resolveBtnLoading(flist)}
					loadingText="上传中..."
				>
					upload
				</Button>
			)}
			{uploadMode === "img" && (
				<ImgUpload onClick={handleClick}>
					<Icon icon="plus"></Icon>
				</ImgUpload>
			)}
```

这样就效果不错了。

下面制作上传上限，上传上限思路就是到了设置的值，直接不显示上传按钮即可。上传上限在单按钮模式没啥意义，所以在有进度条列表时根据进度条列表来看。列表元素大于设定的值就不显示。

```
	/** 允许上传最大容量*/
	max?: number;
```

使用变量来解决是否显示：

```
const shouldShow = useMemo(() => {
		if (max !== undefined) {
			return flist.length < max;
		} else {
			return true;
		}
	}, [max, flist]);
```

渲染部分加上条件：

```
	{shouldShow && uploadMode === "default" && (
				<Button
					onClick={handleClick}
					isLoading={resolveBtnLoading(flist)}
					loadingText="上传中..."
				>
					upload
				</Button>
			)}
			{shouldShow && uploadMode === "img" && (
				<ImgUpload onClick={handleClick}>
					<Icon icon="plus"></Icon>
				</ImgUpload>
			)}
```

就完成了。可以测试下，是不是有效。

下面制作限定后缀名，与多重选择。

这个是input的accept字段与multiple字段

mdn : https://developer.mozilla.org/zh-CN/docs/Web/HTML/Element/Input/file

老方法，暴露出去，配置初始值就ok。

```
/** input的accept属性 */
	accept?: string;
	/** input的multiple属性   multiple为true和max冲突*/
	multiple?: boolean;
```

```
		<input
				ref={inputRef}
				type="file"
				onChange={handleChange}
				style={{ display: "none" }}
				value=""
				multiple={multiple}
				accept={accept}
			></input>
```

下面制作图片裁切功能，这个功能我们需要在上传前插进去，然后返回一裁剪好的file，进行后续操作。同时，还要利用下前面写的modal组件。

先约定出现条件为不为multiple并且为img时。后续有需要可以暴露个配置项单独设定。

```
	const showSlice =useMemo(()=>{
		if(!multiple&&uploadMode==='img'){
			return true
		}else{
			return false
		}
	},[multiple,uploadMode])
```

modal的话，里面内容是个动态的，所以我们要存个变量单独设置。

modal的内容需要读取文件后回显，所以我们需要复用imagelist里方法，把那个方法提取出来做个函数：

```typescript
const getBase64=(raw:File,callback:Function)=>{
	const reader = new FileReader();
	reader.addEventListener("load", () => {
		callback(reader.result)
	});
	reader.readAsDataURL(raw);
}
```

这样modal使用此方法也可以显示图片了。

我们先完成不影响逻辑情况下弹框，关闭弹框后执行后续操作。

先将前面写的逻辑用fn包裹起来，装进state里，等待modal的回调调用它：

```
			const restfn = () => {
				if (beforeUpload) {
					const p = beforeUpload(f, i);
					if (p instanceof Promise) {
						p.then((res: File) => {
							postData(
								res,
								resolveFilename(uploadFilename, i),
								axiosConfig!,
								i,
								onProgress,
								setFlist,
								successCallback,
								failCallback
							);
						});
					} else {
						if (p) {
							//false不执行
							postData(
								f,
								resolveFilename(uploadFilename, i),
								axiosConfig!,
								i,
								onProgress,
								setFlist,
								successCallback,
								failCallback
							);
						}
					}
				} else {
					postData(
						f,
						resolveFilename(uploadFilename, i),
						axiosConfig!,
						i,
						onProgress,
						setFlist,
						successCallback,
						failCallback
					);
				}
			};
			setResCallback({ restfn });
			if (showSlice) {
				setModalOpen(true);
				showModalToSlice(f, setModalContent).then((r) => {
					f = r;
				});
			} else {
				restfn()
			}
```

这里需要3个状态，modal开启关闭，图片string，以及上面包裹的callback 。

```
	const [modalOpen, setModalOpen] = useState(false);
	const [modalContent, setModalContent] = useState<string>("");
	const [rescallback, setResCallback] = useState<{ restfn: Function }>({
		restfn: () => {},
	});
```

先直接返回原file，并且显示图片:

```
const showModalToSlice = function(
	f: File,
	setModalContent: React.Dispatch<any>
): Promise<File> {
	const p = new Promise<File>((res) => {
		getBase64(f, (s: string) => {
			setModalContent(s);
			res(f);
		});
	});
	return p;
};
```

渲染部分：

```
	<Modal
				callback={() => {
					if (rescallback.restfn) rescallback.restfn();
				}}
				maskClose={false}
				closeButton={false}
				visible={modalOpen}
				parentSetState={setModalOpen}
			>
				<div>
					<img
						style={{ width: "100%", height: "100%" }}
						src={modalContent}
						alt="modalpic"
					></img>
				</div>
				<div>
					<button>放大</button>
					<button>缩小</button>
					<button>旋转</button>
				</div>
			</Modal>
```

如果上传图片正常弹框，并且点确认或者取消会继续逻辑就ok。

下面就要把图片替换进行裁切。原理采用张老师课上原理。

将forEach的函数进行修改，里面restfn可以传参，用来替换掉file，后续裁切好只要传file给这个函数就能完成上传操作：

```typescript
	filelist.forEach((f, i) => {
			//裁剪会改变file
			const restfn = (f: File) => {
				if (beforeUpload) {
					const p = beforeUpload(f, i);
					if (p instanceof Promise) {
						p.then((res: File) => {
							postData(
								res,
								resolveFilename(uploadFilename, i),
								axiosConfig!,
								i,
								onProgress,
								setFlist,
								successCallback,
								failCallback
							);
						});
					} else {
						if (p) {
							//false不执行
							postData(
								f,
								resolveFilename(uploadFilename, i),
								axiosConfig!,
								i,
								onProgress,
								setFlist,
								successCallback,
								failCallback
							);
						}
					}
				} else {
					postData(
						f,
						resolveFilename(uploadFilename, i),
						axiosConfig!,
						i,
						onProgress,
						setFlist,
						successCallback,
						failCallback
					);
				}
			};
			setResCallback({ restfn });
			if (showSlice) {
				setModalOpen(true);
				showModalToSlice(f, canvasRef, modalContent);
			} else {
				restfn(f);
			}
		});
```

修改showModalToslice函数，我们需要创建个canvas，绘制图片还需要结合img，就是HTMLimg对象，以及canvas的一些参数，我们要做放大缩小旋转功能，所以需要有个变量保存这些状态：

```
type ModalContentType = {
	rotate: number;
	times: number;
	img: HTMLImageElement;
};
```

这个img就是等会canvas借助它来生成图像。

```
const [modalContent, setModalContent] = useState<ModalContentType>({
		rotate: 0,
		times: 1,
		img: new Image(),
	});
```

showModalToSlice，这里用不用promise都无所谓了，因为后面没取then，后续逻辑会通过modal的回调，只要将所需状态保存好即可。

```
const showModalToSlice = function(
	f: File,
	canvasRef: React.RefObject<HTMLCanvasElement>,
	modalContent: ModalContentType
) {
	getBase64(f, (s: string) => {
		const canvas = canvasRef.current;
		if (canvas) {
			modalContent.img.src = s;
			modalContent.img.onload = () => {
				cavasDraw(modalContent, canvas);
			};
		}
	});
};
```

canvasDraw方法就是通过保存的变量以及canvas进行画图：

```typescript
const cavasDraw = function(
	modalContent: ModalContentType,
	canvas: HTMLCanvasElement
) {
	const image = modalContent.img;
	const ctx = canvas.getContext("2d");
	// eslint-disable-next-line no-self-assign
	canvas.height = canvas.height; //清屏
	let imgWidth = image.width;
	let imgHeight = image.height;
	//canvas宽高300,判断图片长宽谁长，取长的
	const times = modalContent.times;
	if (imgWidth > imgHeight) {
		//如果宽比高度大
		let rate = canvas.width / imgWidth;
		imgWidth = canvas.width * times; //让宽度等于canvas宽度
		imgHeight = imgHeight * rate * times; //然后让高度等比缩放
	} else {
		let rate = canvas.height / imgHeight;
		imgHeight = canvas.height * times;
		imgWidth = imgWidth * rate * times;
	}
	//此时，长宽已等比例缩放，算起始点位偏移，起始高度就是canvas高-图片高再除2 宽度同理
	const startX = (canvas.width - imgWidth) / 2;
	const startY = (canvas.height - imgHeight) / 2;
	//旋转操作
	//旋转首先移动原点到图片中心，这里中心是canvas中心,然后再移动回来
	const midX = canvas.width / 2;
	const midY = canvas.height / 2;
	ctx?.translate(midX, midY);

	ctx?.rotate(modalContent.rotate);
	ctx?.drawImage(image, startX - midX, startY - midY, imgWidth, imgHeight);
	ctx?.translate(0, 0);
};
```

旋转操作有点特别，需要找图片中心，然后修改原点，之后再减回来。

渲染部分modal :

```tsx
	<Modal
				title="图片裁剪"
				callback={(v: boolean) => {
					if (v) {
						//如果取消，不执行后续上传
						canvasRef.current!.toBlob(function(blob) {
							if (rescallback.restfn) rescallback.restfn(blob);
						});
					}
					//清除旋转和倍数
					setModalContent({ ...modalContent, rotate: 0, times: 1 });
				}}
				maskClose={false}
				closeButton={false}
				visible={modalOpen}
				parentSetState={setModalOpen}
			>
				<div>
					<canvas
						width={300}
						height={300}
						style={{ width: "100%", height: "100%" }}
						ref={canvasRef}
					>
						　您的浏览器不支持Canvas
					</canvas>
				</div>
				<div>
					<button
						onClick={() => {
							let newContent = {
								...modalContent,
								...{ times: modalContent.times + 0.1 },
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						放大
					</button>
					<button
						onClick={() => {
							let newContent = {
								...modalContent,
								...{ times: modalContent.times - 0.1 },
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						缩小
					</button>
					<button
						onClick={() => {
							let newContent = {
								...modalContent,
								...{ rotate: modalContent.rotate - 0.1 },
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						左旋
					</button>
					<button
						onClick={() => {
							let newContent = {
								...modalContent,
								...{ rotate: modalContent.rotate + 0.1 },
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						右旋
					</button>
					<button
						onClick={() => {
							let newContent = {
								...modalContent,
								rotate: 0,
								times: 1,
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						重置
					</button>
				</div>
			</Modal>
```

这样就制作完成了。可以试下能正常放大缩小重置裁剪就ok。

下面制作鼠标拖动，鼠标事件是由下面这3个事件产生的：

```
		<div
					onMouseDown={handleMouseDown}
					onMouseMove={handleMouseMove}
					onMouseUp={handleMouseUp}
				>
					<canvas
						width={300}
						height={300}
						style={{ width: "100%", height: "100%" }}
						ref={canvasRef}
					>
						　您的浏览器不支持Canvas
					</canvas>
				</div>
```

由于这个操作也要改变draw里绘制行为，所以变量仍放到modalContent里。

我们会有个偏移值x与y，以及是否按下鼠标左键的状态。

这里会有个边界问题，所以我们还需要加个事件，onMouseLeave事件。

首先，增加left , top 偏移，以及moseActive控制是否按下左键，以及按下左键初始坐标：

```
const [modalContent, setModalContent] = useState<ModalContentType>({
		rotate: 0,
		times: 1,
		img: new Image(),
		left: 0,
		top: 0,
	});
	const [mouseActive, setMouseActive] = useState(false);
	const [startXY, setStartXY] = useState({ X: 0, Y: 0 });
```

对canvas外层div进行事件绑定，绑定4个事件：

```typescript
	const handleMouseDown = (
		e: React.MouseEvent<HTMLDivElement, MouseEvent>
	) => {
		setMouseActive(true);
		setStartXY({
			X: e.clientX - modalContent.left,
			Y: e.clientY - modalContent.top,
		});
	};
	const handleMouseMove = (
		e: React.MouseEvent<HTMLDivElement, MouseEvent>
	) => {
		if (mouseActive) {
			let diffX = e.clientX - startXY.X;
			let diffY = e.clientY - startXY.Y;
			let newContent = { ...modalContent, left: diffX, top: diffY };
			setModalContent(newContent);
			cavasDraw(newContent, canvasRef.current!);
		}
	};
	const handleMouseUp = () => {
		setMouseActive(false);
	};
	const handleMouseLeave = () => {
		setMouseActive(false);
	};
```

canvasDraw函数里，只要加上偏移值就ok:

```typescript
	ctx?.drawImage(
		image,
		startX - midX + modalContent.left,
		startY - midY + modalContent.top,
		imgWidth,
		imgHeight
	);
```

再修改下重置的onClick：

```tsx
	<button
						onClick={() => {
							let newContent = {
								...modalContent,
								rotate: 0,
								times: 1,
								left: 0,
								top: 0,
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						重置
					</button>
```

这样就完成了鼠标拖动图片并且可以裁剪上传了。

## 美化部分

对于不同上传结果，列表并没有很好的显示。

首先就是要修改2个上传列表，文件列表在成功后标识绿色，失败标识红色之类。可以复用badge组件制作也可以单独制作，这里我直接就改个文字颜色单独制作下：

```typescript
type ProgressBarStatus = "ready" | "success" | "failed" | "upload";

function chooseProgressListColor(status: ProgressBarStatus) {
	switch (status) {
		case "failed":
			return color.negative;
		case "ready":
			return color.warning;
		case "success":
			return color.positive;
		case "upload":
			return color.secondary;
	}
}
```

styled制作个随status变化的div：

```typescript
const ProgressListItemName = styled.div<{ status: ProgressBarStatus }>`
	color: ${(props) => chooseProgressListColor(props.status)};
`;
```

渲染里Icon也可以这么制作：

```tsx
function UploadList(props: UploadListProps) {
	const { flist, onRemove } = props;
	return (
		<ul style={{ padding: "10px" }}>
			{flist.map((item) => {
				return (
					<ProgressLi key={item.uid}>
						<ProgressListItem>
							<ProgressListItemName status={item.status}>
								{item.filename}
							</ProgressListItemName>
							<div>
								<Button
									style={{
										padding: "0",
										background: "transparent",
									}}
									onClick={() => onRemove(item)}
								>
									<Icon
										icon="close"
										color={chooseProgressListColor(
											item.status
										)}
									></Icon>
								</Button>
							</div>
						</ProgressListItem>

						{(item.status === "upload" ||
							item.status === "ready") && (
							<Progress count={item.percent}></Progress>
						)}
					</ProgressLi>
				);
			})}
		</ul>
	);
}
```

这样效果就可以了，下面制作图片列表部分。

图片列表上传途中没有进度条，可以拿个Icon表示正在上传，失败拿个Icon表示失败，只有成功状态才显示图片。

首先上传过程中那个旋转的sync 做个wrapper:

```
export const IconSpin = styled.span`
	&>svg{
		${css`
			animation: ${iconSpin} 2s linear infinite;
		`}
	}
}
`;
```

imglist对对应状态进行修改：

```tsx
export function ImageList(props: imageListProps) {
	const { flist, onRemove, setFlist } = props;
	useMemo(() => {
		if (flist) {
			flist.forEach((item) => {
				if (item.raw && !item.img) {
					//如果有文件并且没有img地址，生成blob地址
					getBase64(item.raw, (e: string) => {
						updateFilist(setFlist, item, {
							img: e || "error",
						});
					});
				}
			});
		}
	}, [flist, setFlist]);
	return (
		<React.Fragment>
			{flist.map((item) => {
				return (
					<span key={item.uid}>
						<ImgWrapper>
							{item.status === "success" && (
								<img
									src={item.img as string}
									alt="upload img"
								></img>
							)}
							{(item.status === "ready" ||
								item.status === "upload") && (
								<IconSpin>
									<Icon
										icon="sync"
										color={color.warning}
									></Icon>
								</IconSpin>
							)}
							{item.status === "failed" && (
								<Icon
									icon="photo"
									color={color.negative}
								></Icon>
							)}
							<ImgCloseBtn
								className="closebtn"
								onClick={() => onRemove(item)}
							>
								<Icon icon="trash" color={color.light}></Icon>
							</ImgCloseBtn>
						</ImgWrapper>
					</span>
				);
			})}
		</React.Fragment>
	);
}
```

这样就可以了，下面美化裁剪图片环节。

我们可以把所有按钮替换成我们做的按钮，里面都做成一个图标：

```
const btnStyle = {
	padding: "10px",
};
const rotateBtnStyle = {
	padding: "10px",
	transform: "rotateY(180deg)",
};
```

```tsx
	<div style={{ marginTop: "10px" }}>
					<Button
						appearance="primary"
						style={btnStyle}
						onClick={() => {
							let newContent = {
								...modalContent,
								...{ times: modalContent.times + 0.1 },
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						<Icon icon="zoom" color={color.light}></Icon>
					</Button>
					<Button
						appearance="primary"
						style={btnStyle}
						onClick={() => {
							let newContent = {
								...modalContent,
								...{ times: modalContent.times - 0.1 },
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						<Icon icon="zoomout" color={color.light}></Icon>
					</Button>
					<Button
						appearance="primary"
						style={btnStyle}
						onClick={() => {
							let newContent = {
								...modalContent,
								...{ rotate: modalContent.rotate - 0.1 },
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						<Icon icon="undo" color={color.light}></Icon>
					</Button>
					<Button
						appearance="primary"
						style={rotateBtnStyle}
						onClick={() => {
							let newContent = {
								...modalContent,
								...{ rotate: modalContent.rotate + 0.1 },
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						<Icon icon="undo" color={color.light}></Icon>
					</Button>
					<Button
						appearance="primary"
						style={btnStyle}
						onClick={() => {
							let newContent = {
								...modalContent,
								rotate: 0,
								times: 1,
								left: 0,
								top: 0,
							};
							setModalContent(newContent);
							cavasDraw(newContent, canvasRef.current!);
						}}
					>
						<Icon icon="zoomreset" color={color.light}></Icon>
					</Button>
				</div>
```

给canvas加个边框：

```tsx
	<canvas
						width={300}
						height={300}
						style={{
							width: "100%",
							height: "100%",
							border: "1px dashed #ff4785",
						}}
						ref={canvasRef}
					>
						　您的浏览器不支持Canvas
					</canvas>
```

暴露自定义上传按钮：

```
	/** 用户自定义按钮 */
	customBtn?:ReactNode;
```

```tsx
			{shouldShow && uploadMode === "default" && (
				<span onClick={handleClick}>
					{customBtn ? (
						customBtn
					) : (
						<Button
							isLoading={resolveBtnLoading(flist)}
							loadingText="上传中..."
						>
							upload
						</Button>
					)}
				</span>
			)}
```

还有些细节可以修修，这样就完成了。

## 编写story

做个knob 再制作个img模式和progress模式上传就ok：

```tsx
import React from "react";
import { Upload } from "./index";
import {
	withKnobs,
	text,
	boolean,
	select,
	number,
} from "@storybook/addon-knobs";
import { Method, AxiosRequestConfig } from "axios";
import { action } from "@storybook/addon-actions";

export default {
	title: "Upload",
	component: Upload,
	decorators: [withKnobs],
};

const methods: Method[] = [
	"get",
	"GET",
	"delete",
	"DELETE",
	"head",
	"HEAD",
	"options",
	"OPTIONS",
	"post",
	"POST",
	"put",
	"PUT",
	"patch",
	"PATCH",
	"link",
	"LINK",
	"unlink",
	"UNLINK",
];

export const knobsUpload = () => {
	const uploadMode = select("uploadMode", ["default", "img"], "default");
	const axiosConfig: Partial<AxiosRequestConfig> = {
		url: text("url", "http://localhost:51111/user/uploadAvatar/"),
		method: select("method", methods, "post"),
	};
	const uploadFilename = text("uploadFilename", "avatar");

	return (
		<Upload
			multiple={boolean("multiple", false)}
			accept={text("accept", "*")}
			slice={boolean("slice", true)}
			progress={boolean("progress", false)}
			max={number("max", 100)}
			onProgress={action("onProgress")}
			onRemoveCallback={action("onRemoveCallback")}
			uploadFilename={uploadFilename}
			axiosConfig={axiosConfig}
			uploadMode={uploadMode}
		></Upload>
	);
};

export const imgUpload = () => <Upload uploadMode="img"></Upload>;

export const progressUpload = () => <Upload progress={true}></Upload>;
```

## 今日作业

完成upload组件。